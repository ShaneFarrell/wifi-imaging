from .simulator import Simulator
import tensorflow as tf
from .image_processing_effects import erode, blur, dilate, gaussian_blur


class Model:
    def __init__(self, image_shape, scene_bbox, rssi_batch_size, access_points, learning_rate=3.75, static_image=None):
        """
        :param image_shape: The (width, height) of the input/output image in pixels
        :param scene_bbox: The (x1, y1, x2, y2) of the scene to show in the image
        :param rssi_batch_size: The number of rssis to process as one batch
        :param access_points: A list of access points associated with each rssi
        """

        tf.reset_default_graph()

        # Set basic variables
        self.image_shape = image_shape
        self.scene_bbox = scene_bbox
        self.rssi_batch_size = rssi_batch_size
        self.access_points = access_points
        self.can_train = static_image is None
        self.post_process_factor = tf.placeholder_with_default(0.0, [])
        self.epoch = 0

        # Create input
        if static_image is None:
            self.input, self.raw_input = self.create_input_variable()
        else:
            self.input = tf.constant(static_image, dtype=tf.float32)
            self.raw_input = self.input

        # Create Simulator
        self.simulator = Simulator(
            1 - self.input,  # FIXME
            scene_bbox,
            access_points,
            rssi_batch_size
        )

        self.rssi_cost = self.create_rssi_cost()
        # self.regularisation_cost = tf.reduce_mean(self.input) * 6000
        # self.regularisation_cost = tf.reduce_max(self.input) * 300
        self.regularisation_cost = self.get_regularisation_cost()

        self.cost = self.rssi_cost + tf.maximum(1.0, self.regularisation_cost)
        optimizer = tf.train.AdamOptimizer(learning_rate)

        # post_process_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "post_process/")
        input_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "model_input/")

        if self.can_train:
            self.trainer = optimizer.minimize(self.cost, var_list=input_vars)
        # self.post_process_trainer = optimizer.minimize(self.cost, var_list=post_process_vars)

        self.session = None

    def get_regularisation_cost(self):
        input_ = tf.expand_dims(tf.expand_dims(self.input, 0), -1)
        blurred = tf.pad(input_, [[0, 0], [1, 1], [1, 1], [0, 0]], "CONSTANT")
        blurred = tf.nn.avg_pool(blurred, [1, 3, 3, 1], [1, 1, 1, 1], "VALID")
        cst = tf.square(blurred - input_) / tf.square(input_)
        cst = tf.reduce_sum(cst)

        # cst += tf.reduce_sum(self.input) / 100
        # cst += tf.reduce_sum(tf.cast(self.input > 0.25, tf.float32) * self.input) / 50
        # cst += tf.reduce_sum(tf.cast(self.input < 0.1, tf.float32) * self.input) / 50
        # cst += tf.reduce_sum(tf.cast(self.input < 0.03, tf.float32) * self.input) / 3
        cst += tf.reduce_sum(self.input[0, :]) * 20
        cst += tf.reduce_sum(self.input[-1, :]) * 20
        cst += tf.reduce_sum(self.input[:, 0]) * 20
        cst += tf.reduce_sum(self.input[:, -1]) * 20
        return cst * 10

    def __enter__(self):
        print("ENTER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        if self.session is None:
            session = tf.Session().__enter__()
            session.run(tf.global_variables_initializer())
            session.graph.finalize()
            self.session = session

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.session is not None:
            self.session.__exit__(exc_type, exc_val, exc_tb)
            self.session = None

    def run(self, rssi_batch, output_variables, post_process_factor=0):
        if self.session is None:
            raise RuntimeError("model session not active")

        self.epoch += 1

        if self.can_train:
            # if post_process_factor > 0:
            #     train = self.post_process_trainer
            # else:
            train = self.trainer
        else:
            train = self.input

        return self.session.run(
            [train, output_variables],
            feed_dict={
                self.simulator.rssis: rssi_batch,
                self.post_process_factor: post_process_factor,
                self.simulator.rssis_exists: rssi_batch != 0
            }
        )[1]

    def create_input_variable(self):
        mutable_input = tf.get_variable(
            "model_input/raw",
            dtype=tf.float32,
            initializer=tf.random_normal(self.image_shape, mean=-20, stddev=3)
        )

        mutable_input = tf.sigmoid(mutable_input / 5)
        mutable_input = dilate(mutable_input, [3, 3])
        # mutable_input = gaussian_blur(mutable_input, 5, 0.0, 2.0)

        for _ in range(4):
            mutable_input = erode(mutable_input, [3, 3])
            # mutable_input = blur(mutable_input, [3, 3])
            mutable_input = gaussian_blur(mutable_input, 2, 0.0, 0.95)

        # for _ in range(4):
        #     mutable_input = mutable_input + (mutable_input - gaussian_blur(mutable_input, 2, 0.0, 0.95))

        # repeat_factor = 0
        # image_size = sum(self.image_shape) / 2
        # for _ in range(int(round(image_size * repeat_factor))):
        #     mutable_input = erode(mutable_input, [3, 3])
        #     mutable_input = blur(mutable_input, [3, 3])

        # mutable_input += 0.01 * tf.get_variable("model_input/raw_bias", dtype=tf.float32, initializer=1.0)
        raw_input = mutable_input
        # post_process_image = self.get_post_process_input(mutable_input)
        # mutable_input = mutable_input + post_process_image * self.post_process_factor

        # mutable_input = mutable_input ** 2
        mutable_input = erode(mutable_input, [3, 3])
        mutable_input = gaussian_blur(mutable_input, 2, 0.0, 0.95)

        # mutable_input = mutable_input - self.post_process_factor
        mutable_input = tf.maximum(mutable_input, 0)
        mutable_input = tf.minimum(mutable_input, 1)

        return mutable_input, raw_input

    def create_rssi_cost(self):
        dist_maps = self.simulator.get_square_distance_maps()
        costs = tf.reduce_min(dist_maps, [1, 2, 3])
        rssi_cost = tf.reduce_mean(costs, 0)
        return rssi_cost

    def get_post_process_input(self, network_input):
        network = network_input
        # network = self.post_process_layer(network, 128, 3, "3_1")
        # network = self.post_process_layer(network, 64, 45, "45_1")
        # network = self.post_process_layer(network, 32, 3, "3_2")
        for i in range(3):
            network = self.post_process_layer(network, 64, 5, i)
        # network = tf.sigmoid(network)
        network = tf.tanh(network)
        network = network - tf.reduce_mean(network)
        # network = tf.minimum(network, tf.reduce_max(network_input))
        # network /= tf.reduce_mean(network)
        # network *= tf.reduce_mean(network_input)

        return network

    def post_process_layer(self, network, filters, size, layer_name):
        network = tf.expand_dims(tf.expand_dims(network, 0), 0)
        network = tf.layers.conv2d(
            network,
            filters=filters,
            kernel_size=size,
            padding="same",
            activation=tf.nn.leaky_relu,
            data_format="channels_first",
            name=f"post_process/{layer_name}/conv"
        )

        network = -tf.layers.max_pooling2d(
            -network,
            pool_size=3,
            strides=1,
            padding="same",
            data_format="channels_first",
            name=f"post_process/{layer_name}/max"
        )
        return network[0, 0]
