from typing import Tuple
from math import pi
import tensorflow as tf
from .transformation import meters_to_pixels


class AccessPoint:
    def __init__(self, position: Tuple[float, float], frequency=2.4, signal_radius=128, signal_map_shape=(512, 512)):
        """

        :param position: Position in meters, (0, 0) is the center of the scene
        """
        self.position_meters = position
        self.frequency = frequency
        self.signal_radius = signal_radius
        self.signal_map_shape = signal_map_shape

    def get_position_pixels(self, image_shape, scene_bbox):
        return meters_to_pixels(image_shape, scene_bbox, self.position_meters)

    def get_signal_map(self):
        frequency = self.frequency * 1_000_000_000.0
        light_speed = 299_792_458
        base_dist = 0.1  # FIXME

        delta = (self.signal_radius - base_dist) / self.signal_map_shape[1]
        dist = tf.ones(self.signal_map_shape) * tf.expand_dims(
            tf.range(base_dist, self.signal_radius, delta, dtype=tf.float32),
            -1
        )

        const = 20 * log10(frequency) + 20 * log10((4 * pi) / light_speed) - 6
        # rtn = -(10 * log10(tf.square(dist)) + const)
        rtn = -(10 * log10(tf.pow(dist, 3)) + const)
        from .drawing import show_image
        show_image(rtn, True)
        return rtn

    def get_signal_map(self):
        signal_map_length, signal_map_width = self.signal_map_shape
        distance_range = tf.range(signal_map_length, dtype=tf.float32) / signal_map_length
        meters_radius = 4.2

        signal_strength_range = -33 + tf.log(distance_range * meters_radius * 2 * 2) * -5

        signal_map = broadcast(signal_strength_range, [..., signal_map_width])

        # from .drawing import show_image
        # show_image(signal_map, True)
        return signal_map



def log10(x):
    return tf.log(tf.maximum(x, 10 ** -20)) / tf.log(10.0)


def broadcast(tensor, shape):
    """Return the input tensor tiled to fit the specified shape

    Example:
        broadcast(my_tensor, [10, ..., 5, 5])

    The ellipsis are required and are used to represent the shape of the base
    tensor

    :param tensor: The tensor to broadcast to the new shape
    :param shape: A list of dimensions. Must contain an Ellipsis
    :return: The input tensor expanded and tiled to fit the new shape
    """
    reshape_shape = []
    tile_shape = []

    for x in shape:
        if x == Ellipsis:
            reshape_shape.extend(tensor.shape)
            tile_shape.extend([1 for _ in tensor.shape])
        else:
            reshape_shape.append(1)
            tile_shape.append(x)

    return tf.tile(tf.reshape(tensor, reshape_shape), tile_shape)