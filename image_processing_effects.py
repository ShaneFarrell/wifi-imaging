import tensorflow as tf


def blur(image, filter_shape):
    return remove_padding(tf.nn.pool(
        add_padding(image),
        [*filter_shape, 1],
        "AVG", "SAME"
    ))


def gaussian_kernel(size: int, mean: float, std: float):
    """Makes 2D gaussian Kernel for convolution."""

    d = tf.distributions.Normal(mean, std)
    vals = d.prob(tf.range(start=-size, limit=size + 1, dtype=tf.float32))
    gauss_kernel = tf.einsum('i,j->ij', vals, vals)

    return gauss_kernel / tf.reduce_sum(gauss_kernel)


def gaussian_blur(image, size, mean, std):
    gauss_kernel = add_padding(gaussian_kernel(size, mean, std), 0, 2)
    # from .drawing import show_image
    # show_image(gauss_kernel[..., 0, 0], True)
    return tf.nn.conv2d(add_padding(image, 1, 1), gauss_kernel, strides=[1, 1, 1, 1], padding="SAME")[0, ..., 0]


def dilate(image, filter_shape):
    return remove_padding(tf.nn.pool(
        add_padding(image),
        [*filter_shape, 1],
        "MAX", "SAME"
    ))


def erode(image, filter_shape):
    return -dilate(-image, filter_shape)


def add_padding(image, start=1, end=2):
    for _ in range(start):
        image = tf.expand_dims(image, 0)

    for _ in range(end):
        image = tf.expand_dims(image, -1)

    return image


def remove_padding(image):
    return image[0, ..., 0, 0]
