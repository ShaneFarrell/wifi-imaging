import numpy as np
from scipy.ndimage.morphology import distance_transform_edt, binary_dilation


def get_average_wall_distance(ground_truth, estimate, pixel_to_meters=(1, 1)):
    """
    1) Take the distance transform of the ground truth.
    2) Use the estimate as a mask on the distance transform.
    3) Sum the masked value and divide it by the sum of the mask itself (the estimate).

    :param ground_truth: A binary image
    :param estimate: A binary image
    :param pixel_to_meters: (x_scale, y_scale) how many meters each pixel represents
    :return: The average distance in pixels or meters
    """

    distance_transform = distance_transform_edt(ground_truth, sampling=pixel_to_meters)
    return np.sum(distance_transform * estimate) / np.sum(estimate)


def recall(estimate, ground_truth):
    return np.sum(ground_truth * estimate) / np.sum(ground_truth)


def evaluate(building, estimate, image_shape):
    ground_truth = building.binary_ground_truth
    x1, y1, x2, y2 = building.scene_bbox
    x_scale = (x2 - x1) / image_shape[0]
    y_scale = (y2 - y1) / image_shape[1]

    return max(
        get_average_wall_distance(ground_truth, estimate, [x_scale, y_scale]),
        get_average_wall_distance(estimate, ground_truth, [x_scale, y_scale])
    )


def show_eval(building, estimate, image_shape):
    ground_truth = building.binary_ground_truth
    x1, y1, x2, y2 = building.scene_bbox
    x_scale = (x2 - x1) / image_shape[0]
    y_scale = (y2 - y1) / image_shape[1]
    transform = distance_transform_edt(ground_truth, sampling=(x_scale, y_scale))

    import matplotlib.pyplot as plt
    plt.imshow(transform, cmap="plasma")
    plt.show()
    plt.imshow(transform, cmap="plasma")
    plt.show()
    plt.imshow(transform * estimate, cmap="plasma")
    plt.show()
    plt.imshow(estimate, cmap="plasma")
    plt.show()
    plt.imshow(~ground_truth, cmap="plasma")
    plt.show()