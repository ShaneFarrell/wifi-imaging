import numpy as np
from typing import List, Tuple
import matplotlib.pyplot as plt
from random import uniform
from skimage.draw import line_aa
import tensorflow as tf
from PIL import Image, ImageDraw

LINES = List[Tuple[float, ...]]
POINTS = List[Tuple[float, ...]]


def sigmoid(x):
    return 1/(1+np.exp(-x))


def draw_tick_lines(shape, lines: LINES, thickness):
    img = Image.new('L', shape, 0)
    draw = ImageDraw.Draw(img)
    for x1, y1, x2, y2 in lines:
        draw.line([(x1, y1), (x2, y2)], fill=255, width=thickness)

    return np.asarray(img, float).T / 255


def draw_line(shape, x1, y1, x2, y2, blur=1):
    blur = float(blur)
    w, h = shape
    x, y = np.meshgrid(
        np.arange(w, dtype=np.float32),
        np.arange(h, dtype=np.float32),
        indexing="ij"
    )
    m = np.square(x2 - x1) + np.square(y2 - y1)
    line_dist = np.square(x * (y2 - y1) - y * (x2 - x1) + x2 * y1 - y2 * x1) / m
    mean_x = (x1 + x2) / 2
    mean_y = (y1 + y2) / 2
    mean_dist = (np.square(x - mean_x) + np.square(y - mean_y)) * 4
    circle = sigmoid((m - mean_dist) / blur ** 2)
    line = circle * sigmoid(-line_dist / blur) * 2
    return line


def draw_lines(shape, lines: LINES, rectangles=False):
    result = np.zeros(shape, np.float)
    for x1, y1, x2, y2 in lines:
        if rectangles:
            result[int(x1):int(x2), int(y1):int(y2)] = 1
        else:
            xs, ys, values = line_aa(int(x1), int(y1), int(x2), int(y2))
            xs = np.maximum(np.minimum(xs, shape[0] - 1), 0)
            ys = np.maximum(np.minimum(ys, shape[1] - 1), 0)
            result[xs, ys] = values

    return result


def draw_points(shape, points: POINTS):
    result = np.zeros(shape, np.float)

    for x, y in points:
        if 0 < x < shape[0] and 0 < y < shape[1]:
            result[int(x), int(y)] = 1

    return result


def draw_wall_detectors(size):
    detectors = [
        -np.ones([size, size], np.float32) / (size ** 2)
    ]

    segments = size

    for x in range(size - 1):
        # Vertical Lines
        for i in range(1, segments + 1):
            detectors.append(draw_detector(size, x, 0, False, i / segments))

        # Up/Down T-Junctions
        detectors.append(draw_detector(size, x, 0, True, 0.5))
        detectors.append(
            draw_detector(size, size - 1 - x, size - 1, True, 0.5)
        )

        # Crosses
        detectors.append(draw_detector(size, x, 0, True, False))

    for y in range(1, size):
        # Horizontal Lines
        for i in range(1, segments + 1):
            detectors.append(draw_detector(size, 0, y, False, i / segments))

        # Left/Right T-Junctions
        detectors.append(draw_detector(size, 0, y, True, 0.5))
        detectors.append(
            draw_detector(size, size - 1, size - 1 - y, True, 0.5)
        )

    return np.stack(detectors)


def draw_detector(size, x, y, junction, length):
    x2 = int(((size - 1) - x) * length)
    y2 = int((size - 1) - y * length)

    detector = np.zeros([size, size])
    xs, ys, values = line_aa(x, y, x2, y2)
    detector[xs, ys] = values
    if junction:
        opposite_detector = np.zeros([size, size])
        x, y = y, (size - 1) - x
        xs, ys, values = line_aa(x, y, (size - 1) - x, (size - 1) - y)
        opposite_detector[xs, ys] = values
        detector = np.maximum(detector, opposite_detector)
    pos_detector = detector / np.sum(detector)
    neg_detector = (1 - detector) / np.sum(1 - detector)
    return pos_detector * 50 - neg_detector * 50


close_all_counter = 0
last_wh = None
current_plot = None
last_len = None
global_init = tf.global_variables_initializer()


def show_image(images, block=False, value_ranges=None, transpose=False, save_path=None, headers=None):
    global close_all_counter, last_wh, current_plot, last_len
    close_all_counter += 1

    if not isinstance(images, list):
        images = [images]

    min_vals = [None] * len(images)
    max_vals = [None] * len(images)

    if value_ranges is not None:
        for key, value in value_ranges.items():
            min_vals[key] = value[0]
            max_vals[key] = value[1]

    tensor_positions = []
    for i, image in enumerate(images[:]):
        if tf.contrib.framework.is_tensor(image):
            tensor_positions.append(i)

        if len(image.shape) != 2:
            for j, dim_size in enumerate(image.shape[:-1]):
                if image.shape[j + 1] == dim_size and dim_size >= 32:
                    for _ in range(j):
                        images[i] = images[i][0]
                    while len(images[i].shape) > 2:
                        images[i] = images[i][:, :, 0]
                    break
            else:
                raise ValueError(f"Invalid image dimensions {image.shape}")

    if tensor_positions:
        with tf.Session() as session:
            session.run(tf.global_variables_initializer())
            tensors = [images[i] for i in tensor_positions]
            results = session.run(tensors)
            for result, i in zip(results, tensor_positions):
                images[i] = result

    w, h = min(len(images), 5), (len(images) - 1) // 5 + 1

    if close_all_counter > 20 or (w, h) != last_wh or len(images) != last_len:
        plt.close("all")
        close_all_counter = 0
        last_wh = w, h
        last_len = len(images)
        figure, current_plot = plt.subplots(h, w, squeeze=False,
                                            figsize=(w * 3, min(h * 3, 9)))

    for x in range(w):
        for y in range(h):
            i = x + y * w
            if i >= len(images):
                img = np.zeros_like(images[0])
                vmin, vmax = None, None
            else:
                img = images[i]
                vmin, vmax = min_vals[i], max_vals[i]
            if transpose:
                img = img.T
            if headers:
                current_plot[y, x].set_title(headers[0])
                headers = headers[1:]
            current_plot[y, x].imshow(img, "plasma", vmin=vmin, vmax=vmax)

    if save_path is not None:
        plt.savefig(save_path)

    if block is True:
        plt.show()
    elif block:
        plt.pause(block)
    else:
        plt.pause(0.02)
