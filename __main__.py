from .model import Model
from .data import Dataset, aps_bbox_selector
from .drawing import show_image, draw_points
from itertools import cycle, count
from .evaluation import evaluate, show_eval
from pathlib import Path
import numpy as np
from random import shuffle


def main():
    image_shape = (128, 128)
    rssi_batch_size = 256#350
    learning_rate = 2#3.75
    scene_padding = 0.5#0.1#2

    max_epoch = 200#_000 # 500

    show_images_frequency = 1

    with open(r"C:\Users\c12722625\Dropbox\PhD\Projects\WiFi_Imaging\results\out.txt", "w") as fl:
        pass

    # if True:
    #     image_shape = (256, 256)
    #     # image_shape = (512, 512)
    #     rssi_batch_size = 1000

    # Create the dataset
    dataset = Dataset(rssi_batch_size, image_shape, scene_padding)

    for i in range(0, 1):
        dataset.load_building_file(
            fr"C:\Users\c12722625\Dropbox\PhD\Projects\RSSI_3\dataset_brick\building_{i}.npz"
        )

    # At home
    # dataset.load_building_file(
    #     r"C:\Users\c12722625\Dropbox\PhD\Data\House\buildings\single.npz", rectangle_walls=True
    # )

    # At home clean
    # dataset.load_building_file(
    #     r"C:\Users\c12722625\Dropbox\PhD\Data\House\building_clean.npz"
    # )

    # At home 2 walls
    # dataset.load_building_file(
    #     r"C:\Users\c12722625\Dropbox\PhD\Data\House\buildings\double.npz", rectangle_walls=True
    # )

    # At home 2 walls corner
    # dataset.load_building_file(
    #     r"C:\Users\c12722625\Dropbox\PhD\Data\House\buildings\corner.npz", rectangle_walls=True
    # )

    # At home 2 walls distributed
    # dataset.load_building_file(
    #     r"C:\Users\c12722625\Dropbox\PhD\Data\House\buildings\distributed.npz", rectangle_walls=True
    # )

    # # At home 2 walls - no center APs
    # dataset.load_building_file(
    #     r"C:\Users\c12722625\Dropbox\PhD\Data\House\2walls.npz", aps_bbox_selector(0, 2, 5, 5, True)
    # )

    # # At home 2 walls - only center APs
    # dataset.load_building_file(
    #     r"C:\Users\c12722625\Dropbox\PhD\Data\House\2walls.npz", aps_bbox_selector(0, 2, 5, 5)
    # )

    # # At home 2 walls - two APs per room
    # dataset.load_building_file(
    #     r"C:\Users\c12722625\Dropbox\PhD\Data\House\2walls.npz", aps_bbox_selector(1.5, 0, 3.5, 7.5)
    # )
    #
    # # At home 2 walls - one AP per room
    # dataset.load_building_file(
    #     r"C:\Users\c12722625\Dropbox\PhD\Data\House\2walls.npz", aps_bbox_selector(2.5, 0, 3.5, 7.5)
    # )

    # dataset.load_building_file(
    #     r"C:\Users\c12722625\Dropbox\PhD\Projects\RSSI_3\dataset\building_0.npz"
    # )

    # dataset.load_building_file(
    #     r"..\RSSI_3\dataset_brick\building_1.npz"
    # )

    # dataset.load_real_world_data(
    #     r"C:\Users\c12722625\Desktop\results\out\b3test1.npz"
    # )

    # Each building
    for building in dataset.buildings:
        print(*building.raw_bbox[2:])
        print(image_shape[0] / building.scene_shape[0], image_shape[1] / building.scene_shape[1])
        # Create the model
        model = Model(
            image_shape, building.scene_bbox, rssi_batch_size, building.access_points, learning_rate,
        )#static_image=0.1*(building.ground_truth>0.1))

        # Set up the viewer
        # direct_view = [
        #     draw_points(image_shape, building.ap_pixel_positions)
        # ]
        #
        # computed_view = [
        #     # model.simulator.get_unwound_scene_maps()[0],
        #     # model.simulator.get_unwound_smear_maps()[0],
        #     # model.simulator.get_unwound_signal_simulations()[0],
        #     model.simulator.get_signal_simulations()[0],
        #     # model.simulator.get_signal_simulations()[1],
        #     # model.simulator.get_signal_simulations()[2],
        #     # model.simulator.get_signal_simulations()[3],
        #     # model.simulator.get_signal_simulations()[4],
        #     # model.input + building.ground_truth / 2,
        #     # model.input - model.raw_input,
        #     1 / (1 + model.simulator.get_square_distance_maps()[0]),
        #     1/(1+ model.simulator.get_square_distance_rings()[0, 0]**4),
        #     1/(1+ model.simulator.get_square_distance_rings()[0, 1]**4),
        #     # 1/(1+ model.simulator.get_square_distance_rings()[0, 7]**4),
        #     model.simulator.get_location_distribution(),
        # ]

        compute = {
            "algorithm cost value": model.rssi_cost,
            "regularisation cost": model.regularisation_cost
        }

        post_process_start = 60
        post_process_increment = 0.05 / 50
        post_process_cap = 0.05

        # Run the model
        with model:
            for epoch, rssi_batch in enumerate(cycle(building.rssi_batches)):
                # nn_factor = post_process_increment * (epoch - post_process_start)
                # nn_factor = min(max(0.0, nn_factor), post_process_cap)*0
                # nn_factor = 0

                output, estimate = model.run(
                    rssi_batch, [compute, model.input]
                )
                readout_text = "    ".join(f"{k}: {v:10.2f}" for k, v in output.items())

                from .post_process import post_process
                result = post_process(estimate)
                score = evaluate(building, result, image_shape)

                print(f"{epoch:4d}    distance: {score:4.3f}  --  {readout_text}")
                # from scipy.ndimage.morphology import distance_transform_edt
                # dt = distance_transform_edt(building.binary_ground_truth) * result
                if show_images_frequency and ((epoch >= max_epoch) or (epoch % show_images_frequency == 0)):
                    image_save_path = None
                    if epoch >= max_epoch:
                        image_save_directory = r"C:\Users\c12722625\Dropbox\PhD\Projects\WiFi_Imaging\results\Images"
                        for i in count():
                            image_save_path = f"{image_save_directory}/{i}.png"
                            if not Path(image_save_path).exists():
                                break
                    show_image([
                        # np.maximum(building.ground_truth>0, draw_points(image_shape, building.ap_pixel_positions)*0.5),
                        # building.ground_truth + estimate,
                        building.ground_truth > 0,
                        estimate,
                        result
                    ], headers=["Ground Truth", "Raw Output", "Thresholded Output"], save_path=image_save_path)
                # show_image([dt, building.ground_truth, estimate, result, *direct_view, *output_images])
                if epoch >= max_epoch:
                    show_eval(building, result, image_shape)
                    with open(r"C:\Users\c12722625\Dropbox\PhD\Projects\WiFi_Imaging\results\out.txt", "a") as fl:
                        fl.write(f"{image_save_path.split('/')[-1]}|{score}\n")
                    break

            # run = model.run(cycle(building.rssi_batches), [compute, computed_view])
            # for i, (readout, output) in enumerate(run):
            #     post_process_factor = min(max(0, post_process_increment * (i - post_process_start)), post_process_cap)
            #     readout_text = "    ".join(f"{k}: {v:10.2f}" for k, v in readout.items())
            #
            #     print(f"{i:4d}    {post_process_factor:4.3f}    {readout_text}")
            #     show_image([*direct_view, *output])
            #
            #     if i > post_process_start and post_process_factor < post_process_cap:
            #         run.send(post_process_factor)

main()
