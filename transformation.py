import tensorflow as tf
import numpy as np
from math import pi


def meters_to_pixels(image_shape, scene_bbox, position):
    scene_shape = (scene_bbox[2] - scene_bbox[0], scene_bbox[3] - scene_bbox[1])

    position_meters = np.array(position)

    position_meters_relative = position_meters - scene_bbox[:2]
    position_normalised = position_meters_relative / scene_shape
    position_pixels = position_normalised * image_shape

    return position_pixels


def smear(tensor, stride=64):
    """Smears along the first axis."""
    tensor = tf.log(tf.minimum(tf.maximum(tensor, 10**-20), 1.0))
    tensor_lines = tf.unstack(tensor, axis=1)
    signal_lines = [None] * len(tensor_lines)

    for i in range(0, len(tensor_lines), stride):
        signal_lines[i] = tf.reduce_sum(tensor[:, :i + 1], 1)

        for j in range(i - 1, i - stride // 2, -1):
            if j >= 0:
                signal_lines[j] = signal_lines[j + 1] - tensor_lines[j + 1]

        for j in range(i + 1, i + stride):
            if j < len(signal_lines) and signal_lines[j] is None:
                signal_lines[j] = signal_lines[j - 1] + tensor_lines[j]

    return tf.exp(tf.stack(signal_lines, axis=1))


def fast_unwind(tensors, offsets, output_shape, radii):
    """
    Unwinds tensors into spherical coordinates centered around positions.
    tensors: [batch, width, height, channels]

    output: [batch, dist, yaw, pitch, channels]

    dist: 0 -> radius
    yaw: 0 -> 360
    pitch: top pole -> bottom pole
    """
    width, height = output_shape

    radii = tf.reshape(radii, (-1, 1, 1, 1))
    offsets = tf.reshape(offsets, (-1, 2, 1, 1, 1))

    output_x, output_y, _ = tf.meshgrid(
        tf.range(width, dtype=tf.float32),
        tf.range(height, dtype=tf.float32),
        tf.constant(1, dtype=tf.float32),
        indexing="ij"
    )
    output_x = tf.expand_dims(output_x, 0)
    output_y = tf.expand_dims(output_y, 0)

    offset_x, offset_y = tf.unstack(offsets, axis=1)

    distance = (output_x / width) * radii
    yaw = -(output_y / height) * (2 * pi) + pi

    input_x = offset_x + distance * tf.cos(yaw)
    input_y = offset_y - distance * tf.sin(yaw)
    input_z = tf.zeros_like(input_x)

    # with tf.Session() as session:
    #     import matplotlib.pyplot as plt
    #     session.run(tf.global_variables_initializer())
    #     plt.imshow(session.run(input_x)[0, 0, :, :], cmap="plasma")
    #     plt.show()

    flow = tf.stack([input_x, input_y, input_z], axis=4)

    tensors = tf.expand_dims(tf.expand_dims(tf.expand_dims(tensors, 0), -1), -1)
    return fast_interpolated_sample(tensors, flow)


def fast_wind(tensors, offsets, output_shape, radii):
    width, height, depth = tensors.get_shape().as_list()[1:-1]
    output_width, output_height = output_shape

    radii = tf.reshape(radii, (-1, 1, 1, 1))
    offsets = tf.reshape(offsets, (-1, 2, 1, 1, 1))

    output_x, output_y, output_z = tf.meshgrid(
        tf.range(output_width, dtype=tf.float32),
        tf.range(output_height, dtype=tf.float32),
        tf.constant(1, dtype=tf.float32),
        indexing="ij"
    )
    output_x = tf.expand_dims(output_x, 0)
    output_y = tf.expand_dims(output_y, 0)
    output_z = tf.expand_dims(output_z, 0)

    offset_x, offset_y = tf.unstack(offsets, axis=1)
    offset_z = tf.zeros_like(offset_x)

    distance = tf.sqrt(
        tf.square(output_x - offset_x) +
        tf.square(output_y - offset_y) +
        tf.square(output_z - offset_z)
    )
    yaw = tf.atan2(output_y - offset_y, output_x - offset_x) + pi
    pitch = tf.acos((output_z - offset_z) / distance)

    input_x = (distance / radii) * width
    input_y = (yaw / (2 * pi)) * height
    input_z = (pitch / pi) * (depth - 1)

    # with tf.Session() as session:
    #     import matplotlib.pyplot as plt
    #     session.run(tf.global_variables_initializer())
    #     plt.imshow(session.run(input_y)[0, :, :, 0].T, cmap="plasma")
    #     plt.show()

    flow = tf.stack([input_x, input_y, input_z], axis=4)

    # Add wraparound for interpolation from 360 back to 0 degrees
    wrapped_height = tensors.get_shape()[2] + 1
    tensors = tf.tile(tensors, [1, 1, 2, 1, 1])[:, :, :wrapped_height, :, :]

    return fast_interpolated_sample(tensors, flow)


def fast_interpolated_sample(tensors, flow):
    """
    tensors: [batch, width, height, depth, channels]
    flow: [batch, width, height, depth, 3]
    output: [batch, width, height, depth, channels]
    """
    with tf.name_scope("fast_interpolated_sample"):
        batch_size, width, height, depth = flow.get_shape().as_list()[:4]
        channels = tensors.get_shape()[4]
        if tensors.get_shape()[0] == 1:
            tensors = tf.tile(tensors, [batch_size, 1, 1, 1, 1])
        query_points_flattened = tf.reshape(
            flow,
            [batch_size, -1, 3]
        )
        # Compute values at the query points, then reshape the result back to
        # the image grid.
        interpolated = _interpolate_trilinear(tensors, query_points_flattened)

        interpolated = tf.reshape(
            interpolated,
            [batch_size, width, height, depth, channels]
        )

        return interpolated


def _interpolate_trilinear(grid,
                           query_points):
    """Finds values for query points on a grid using trilinear interpolation.

    Args:
      grid: a 5-D float `Tensor` of shape `[batch, w, h, d, channels]`.
      query_points: a 3-D float `Tensor` of N points with shape `[batch, N, 3]`.
      name: a name for the operation (optional).

    Returns:
      values: a 3-D `Tensor` with shape `[batch, N, channels]`

    Raises:
      ValueError: if the indexing mode is invalid, or if the shape of the inputs
        invalid.
    """
    with tf.name_scope('interpolate_trilinear'):
        grid = tf.convert_to_tensor(grid)
        query_points = tf.convert_to_tensor(query_points)
        shape = grid.get_shape().as_list()
        if len(shape) != 5:
            msg = 'Grid must be 5 dimensional. Received size: '
            raise ValueError(msg + str(grid.get_shape()))

        batch_size, width, height, depth, channels = shape
        query_type = query_points.dtype
        grid_type = grid.dtype

        if (len(query_points.get_shape()) != 3 or
                query_points.get_shape()[2].value != 3):
            msg = ('Query points must be 3 dimensional and size 3 in dim 2. '
                   'Received size: ')
            raise ValueError(msg + str(query_points.get_shape()))

        _, num_queries, _ = query_points.get_shape().as_list()

        alphas = []
        floors = []
        ceils = []

        index_order = [2, 1, 0]
        unstacked_query_points = tf.unstack(query_points, axis=2)

        for dim in index_order:
            with tf.name_scope('dim-' + str(dim)):
                queries = unstacked_query_points[dim]

                size_in_indexing_dimension = shape[dim + 1]

                max_floor = tf.cast(size_in_indexing_dimension - 1, query_type)
                min_floor = tf.constant(0.0, dtype=query_type)
                floor = tf.minimum(
                    tf.maximum(min_floor, tf.floor(queries)), max_floor)
                int_floor = tf.cast(floor, tf.int32)
                floors.append(int_floor)
                ceil = tf.minimum(int_floor + 1, tf.cast(max_floor, tf.int32))
                ceils.append(ceil)

                # alpha has the same type as the grid, as we will directly use
                # alpha when taking linear combinations of pixel values from the
                # image.
                alpha = tf.cast(queries - floor, grid_type)
                min_alpha = tf.constant(0.0, dtype=grid_type)
                max_alpha = tf.constant(1.0, dtype=grid_type)
                alpha = tf.minimum(tf.maximum(min_alpha, alpha), max_alpha)

                # Expand alpha to [b, n, 1] so we can use broadcasting
                # (since the alpha values don't depend on the channel).
                alpha = tf.expand_dims(alpha, 2)
                alphas.append(alpha)

        if batch_size * height * width * depth > np.iinfo(np.int32).max / 8:
            error_msg = """The image size or batch size is sufficiently large
                     that the linearized addresses used by array_ops.gather
                     may exceed the int32 limit."""
            raise ValueError(error_msg)

        flattened_grid = tf.reshape(
            grid, [batch_size * height * width * depth, channels]
        )
        batch_offsets = tf.reshape(
            tf.range(batch_size) * height * width * depth, [batch_size, 1])

        # This wraps array_ops.gather. We reshape the image data such that the
        # batch, y, and x coordinates are pulled into the first dimension.
        # Then we gather. Finally, we reshape the output back. It's possible
        # this code would be made simpler by using array_ops.gather_nd.
        def gather(z_coords, y_coords, x_coords, name):
            with tf.name_scope('gather-' + name):
                linear_coordinates = (batch_offsets +
                                      x_coords * depth * height +
                                      y_coords * depth +
                                      z_coords)
                gathered_values = tf.gather(flattened_grid, linear_coordinates)
                return tf.reshape(gathered_values,
                                  [batch_size, num_queries, channels])

        # grab the pixel values in the 8 corners around each query point
        v000 = gather(floors[0], floors[1], floors[2], 'v000')
        v001 = gather(floors[0], floors[1], ceils[2], 'v001')
        v010 = gather(floors[0], ceils[1], floors[2], 'v010')
        v011 = gather(floors[0], ceils[1], ceils[2], 'v011')

        v100 = gather(ceils[0], floors[1], floors[2], 'v100')
        v101 = gather(ceils[0], floors[1], ceils[2], 'v101')
        v110 = gather(ceils[0], ceils[1], floors[2], 'v110')
        v111 = gather(ceils[0], ceils[1], ceils[2], 'v111')

        # now, do the actual interpolation
        with tf.name_scope('interpolate'):
            v00x = alphas[2] * (v001 - v000) + v000
            v01x = alphas[2] * (v011 - v010) + v010
            v0xx = alphas[1] * (v01x - v00x) + v00x

            v10x = alphas[2] * (v101 - v100) + v100
            v11x = alphas[2] * (v111 - v110) + v110
            v1xx = alphas[1] * (v11x - v10x) + v10x

            vxxx = alphas[0] * (v1xx - v0xx) + v0xx

        return vxxx
