import numpy as np
from math import sin, cos, radians, atan2, pi
from .access_points import AccessPoint
from .drawing import draw_lines
from .transformation import meters_to_pixels
from typing import List
from random import uniform
from math import pi

"""
observations: [observation, ap]
ground_truth: [self.image_shape, self.image_shape]
bbox = base on AP positions or something, padding added separately
access_points = [AccessPoint(pos) for pos in ap_positions]
"""


class Dataset:
    def __init__(self, rssi_batch_size, image_shape, scene_padding):
        self.rssi_batch_size = rssi_batch_size
        self.scene_padding = scene_padding
        self.image_shape = image_shape

        self.buildings = []

    def load_building_file(self, file_path, ap_filter=None, rectangle_walls=False):
        building_data = np.load(file_path)
        observations = building_data["observations"]
        walls = building_data["building"]
        shape = building_data["building_shape"]
        ap_positions = building_data["ap_positions"]
        if ap_filter is not None:
            deletions = []
            for i, (x, y) in enumerate(ap_positions):
                if not ap_filter(i, x, y):
                    deletions.append(i)

            observations = np.delete(observations, deletions, 1)
            ap_positions = np.delete(ap_positions, deletions, 0)
        print(observations.shape)
        print(ap_positions.shape)
        # import matplotlib.pyplot as plt
        # plt.scatter(ap_positions[:, 1], ap_positions[:, 0])
        # seen = set()
        # for i, (x, y) in enumerate(ap_positions):
        #     if (x, y) not in seen:
        #         seen.add((x, y))
        #     else:
        #         y += 1
        #     plt.text(x, y, str(i))
        # plt.show()

        # observations /= 2
        # observations[np.random.random(observations.shape) < 0.1] = 0
        # observations[observations > -20] = 0
        # observations[observations < -90] = 0
        # import matplotlib.pyplot as plt
        # data = []
        # rssis = []
        # for rssi in range(-300, 0):
        #     data.append(np.sum((rssi < observations) & (observations < rssi + 1)))
        #     rssis.append(rssi)
        #     # print(rssi, np.sum((rssi < observations) & (observations < rssi + 1)))
        # plt.plot(rssis, data)
        # plt.show()
        # print(np.sum(observations < -90), np.sum(observations > -90), np.sum(observations < 0))

        # shape = [0, 0]
        # for wall in walls:
        #     shape[0] = max(shape[0], wall[0], wall[2])
        #     shape[1] = max(shape[1], wall[1], wall[3])

        # if shape[0] < shape[1]:
        #     offset = (shape[1] - shape[0]) / 2
        #     walls = [(wall[0] + offset, wall[1], wall[2] + offset, wall[3]) for wall in walls]
        #     ap_positions = [(ap[0] + offset, ap[1]) for ap in ap_positions]
        # else:
        #     offset = (shape[0] - shape[1]) / 2
        #     walls = [(wall[0], wall[1] + offset, wall[2], wall[3] + offset) for wall in walls]
        #     ap_positions = [(ap[0], ap[1] + offset) for ap in ap_positions]
        #
        # shape = [max(*shape)] * 2

        origin = (shape[0] / 2, shape[1] / 2)
        rotation = uniform(0, 2 * pi)

        rotated_aps = [self.rotate_point(pos, origin, rotation) for pos in ap_positions]
        rotated_walls = [
            (*self.rotate_point((x1, y1), origin, rotation),
             *self.rotate_point((x2, y2), origin, rotation))
            for x1, y1, x2, y2 in walls
        ]

        # shape = [
        #     max(max(w[0], w[2]) for w in rotated_walls) + min(min(w[0], w[2]) for w in rotated_walls) + 2,
        #     max(max(w[1], w[3]) for w in rotated_walls) + min(min(w[1], w[3]) for w in rotated_walls) + 2
        # ]
        # rotated_walls = [[x + 1 for x in wall] for wall in rotated_walls]

        ground_truth = self.get_building_image_walls(shape, rotated_walls, rectangle_walls)

        # import matplotlib.pyplot as plt
        # from scipy.ndimage import binary_dilation
        #
        # plt.imshow(ground_truth > 0, cmap="plasma")
        # plt.show()
        # for dilation in [1, 3, 5, 10]:
        #     plt.imshow(binary_dilation(ground_truth > 0, iterations=dilation),cmap="plasma")
        #     plt.show()
        # from .drawing import show_image
        # show_image(ground_truth, 0.1)

        access_points = [AccessPoint(pos) for pos in rotated_aps]
        self.buildings.append(Building(self, access_points, observations, ground_truth, (0, 0, *shape)))

    def load_real_world_data(self, file_path, relative_bbox=(0, 0, 1, 1)):
        building_data = np.load(file_path, allow_pickle=True)
        observations = building_data["observations"]
        # bbox = building_data["bbox"]
        ap_positions = building_data["ap_positions"]
        laser = building_data["laser"]

        print(f"Dataset contains {len(ap_positions)} aps.")
        ap_positions, observations = prune_aps(ap_positions, observations)
        print(f"Pruned to {len(ap_positions)} aps.")

        building_pos = (388071.29468398646 + 13, 3952973.343655255 - 18, 54, 'S')
        laser = rotate(laser, [0, 0], 188)
        laser += building_pos[:2]



        # building_pos -= np.mean(laser, axis=0)
        # rot_matrix = np.array([[-0.990, 0.139], [-0.139, -0.990]])
        # print(laser)
        # offset = np.mean(laser, axis=0)
        # np.isnan()
        # laser -= np.mean(laser, axis=0)
        import matplotlib.pyplot as plt
        # print("@_@", np.mean(observations[observations != 0]))
        # ys, xs = np.histogram(observations[observations != 0], bins=40)
        # xs = (xs[:-1] + xs[1:]) / 2
        # plt.plot(xs, ys)
        # plt.show()
        # plt.scatter(laser[::1000, 0], laser[::1000, 1], s=0.5)
        # plt.scatter(ap_positions[:, 0], ap_positions[:, 1])
        # plt.show()

        # laser = np.dot(laser, rot_matrix.T)

        bbox = get_bbox(laser)#(ap_positions)

        # bbox = np.array(bbox)
        relative_bbox = np.array(relative_bbox)
        bbox[::2] = relative_bbox[::2] * (bbox[2] - bbox[0]) + bbox[0]
        bbox[1::2] = relative_bbox[1::2] * (bbox[3] - bbox[1]) + bbox[1]

        ground_truth = self.get_building_image_points(bbox, laser)
        access_points = [AccessPoint(pos) for pos in ap_positions]
        self.buildings.append(Building(self, access_points, observations, ground_truth, bbox))

    def get_scene_bbox(self, raw_building_bbox):
        padding = self.scene_padding
        x1, y1, x2, y2 = raw_building_bbox

        return (x1 - padding, y1 - padding, x2 + padding, y2 + padding)

    def rotate_point(self, point, origin, rotation):
        x, y = (point[0] - origin[0], point[1] - origin[1])
        h, w = origin[1] * 2, origin[0] * 2

        aabb_height = abs(w * sin(rotation)) + abs(h * cos(rotation))
        aabb_width = abs(w * cos(rotation)) + abs(h * sin(rotation))
        aabb_square = max(aabb_height, aabb_width)

        y_scale = h / aabb_square
        x_scale = w / aabb_square

        angle = atan2(y, x) + rotation
        length = ((x * x + y * y) ** 0.5)
        return (origin[0] + length * cos(angle) * x_scale, origin[1] + length * sin(angle) * y_scale)

    def get_building_image_walls(self, raw_building_shape, building_walls_array, rectangles=False):
        scene_bbox = self.get_scene_bbox((0, 0, *raw_building_shape))

        scaled_walls = []
        for wall in building_walls_array:
            x1, y1 = meters_to_pixels(self.image_shape, scene_bbox, wall[:2])
            x2, y2 = meters_to_pixels(self.image_shape, scene_bbox, wall[2:])
            scaled_walls.append((x1, y1, x2, y2))

        image = draw_lines(self.image_shape, scaled_walls, rectangles)
        return image

    def get_building_image_points(self, raw_building_bbox, building_point_array):
        scene_bbox = self.get_scene_bbox(raw_building_bbox)

        image = np.zeros(self.image_shape)
        print(len(building_point_array))
        building_point_array = building_point_array[::1000]

        for i, point in enumerate(building_point_array):
            x, y = meters_to_pixels(self.image_shape, scene_bbox, point)
            print(f"\r{i} / {len(building_point_array)}", end="")
            try:
                if 0 < x < self.image_shape[0] and 0 < y < self.image_shape[1]:
                    image[int(x), int(y)] += 1
            except ValueError:
                pass

        print()
        return image / np.max(image)


class Building:
    def __init__(self, dataset, access_points, rssi_observations, ground_truth, bbox):
        self.ground_truth = ground_truth
        self.raw_bbox = bbox
        self.dataset = dataset
        self.access_points: List[AccessPoint] = access_points
        self.rssi_observations = rssi_observations
        print(f"Scene Shape: {self.scene_shape[0]:4.2f} x {self.scene_shape[1]:4.2f} meters")

    @property
    def binary_ground_truth(self):
        return self.ground_truth < 0.1

    @property
    def rssi_batches(self):
        pos = 0
        size = self.dataset.rssi_batch_size
        while pos + size < len(self.rssi_observations):
            yield self.rssi_observations[pos:pos + size]
            pos += size

    @property
    def scene_bbox(self):
        return self.dataset.get_scene_bbox(self.raw_bbox)

    @property
    def ap_pixel_positions(self):
        return [ap.get_position_pixels(self.dataset.image_shape, self.scene_bbox) for ap in self.access_points]

    @property
    def scene_shape(self):
        x1, y1, x2, y2 = self.scene_bbox
        return (x2 - x1, y2 - y1)


def get_bbox(positions):
    xs = positions[:, 0]
    ys = positions[:, 1]
    return np.array([np.nanmin(xs), np.nanmin(ys), np.nanmax(xs), np.nanmax(ys)])


def get_bbox_size(bbox):
    return (bbox[2] - bbox[0]) * (bbox[3] - bbox[1])


def get_outer_removed(positions, bbox):
    removed_extremities = [
        positions[positions[:, 0] != bbox[0]],
        positions[positions[:, 1] != bbox[1]],
        positions[positions[:, 0] != bbox[2]],
        positions[positions[:, 1] != bbox[3]],
    ]

    sizes = [
        get_bbox_size(get_bbox(array))
        for array in removed_extremities
    ]

    return sorted(zip(sizes, removed_extremities))[0][1]


def rotate(points, origin, degrees_cc):
    points = np.array(points)
    origin = np.array(origin)
    angle = radians(degrees_cc)
    matrix = np.array([[cos(angle), -sin(angle)], [sin(angle), cos(angle)]])
    return (points - origin) @ matrix.T + origin


def prune_aps(ap_positions, observations):
    keep_ids = list(range(len(ap_positions)))
    deviations = np.nanstd(observations, 0)
    keep_ids.sort(key=deviations.__getitem__, reverse=True)
    keep_ids = keep_ids[:30]
    return ap_positions[keep_ids], observations[:, keep_ids]


def aps_bbox_selector(x1, y1, x2, y2, invert=False):
    def ap_filter(i, x, y):
        return (x1 <= x <= x2 and y1 <= y <= y2) != invert
    return ap_filter
