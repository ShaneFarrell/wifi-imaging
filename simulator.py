import tensorflow as tf
import numpy as np
from .transformation import fast_unwind, smear, fast_wind
from math import ceil
import numpy as np
from typing import List, Tuple


def log10(x):
    return tf.log(tf.maximum(x, 10**-20)) / tf.log(10.0)


def cache(fn):
    results = {}

    def wrap(*args):
        try:
            return results[args]
        except KeyError:
            pass
        result = fn(*args)
        results[args] = result
        return result

    return wrap


class Simulator:
    mutable_image: tf.Variable  # [width, height]
    rssis: tf.Tensor  # [observation, ap]
    rssis_exists: tf.Tensor  # [observation, ap]
    ap_signal_maps: tf.Tensor  # [ap, width, height]
    ap_positions: tf.Tensor  # [ap, 3]
    ap_radii: tf.Tensor  # [ap]

    def __init__(self, mutable_image, scene_bbox, access_points, rssi_count):

        self.image_shape = mutable_image.get_shape().as_list()
        self.signal_map_shape = access_points[0].signal_map_shape

        ap_positions = [ap.get_position_pixels(self.image_shape, scene_bbox) for ap in access_points]

        ap_signal_maps = [ap.get_signal_map() for ap in access_points]
        ap_radii = [ap.signal_radius for ap in access_points]

        self.ap_count = len(ap_positions)
        self.ap_positions = tf.constant(np.array(ap_positions), dtype=tf.float32)

        self.mutable_image = mutable_image
        self.rssis = tf.placeholder(tf.float32, [rssi_count, self.ap_count])
        self.rssis_exists = tf.placeholder(tf.float32, [rssi_count, self.ap_count])
        self.ap_signal_maps = tf.stack(ap_signal_maps)
        self.ap_radii = tf.constant(np.array(ap_radii), dtype=tf.float32)

        # self.ring_blur = tf.constant(100.05)#tf.constant(10.5)

        # print(self.ap_signal_maps.get_shape().as_list())

    @cache
    def get_unwound_scene_maps(self):
        """Get the unwound view of the scene map for each AP."""
        return fast_unwind(
            tensors=self.mutable_image,
            offsets=self.ap_positions,
            output_shape=self.signal_map_shape,
            radii=self.ap_radii
        )

    @cache
    def get_unwound_smear_maps(self):
        """Get the unwound scene maps smeared along the distance axis."""
        return smear(self.get_unwound_scene_maps())

    @cache
    def get_unwound_signal_simulations(self):
        """Get the unwound raycast simulation for each AP."""
        smear_maps = self.get_unwound_smear_maps()
        occlusion_maps = smear_maps[..., 0]
        signal_maps = tf.expand_dims(self.ap_signal_maps, -1) + 10 * log10(occlusion_maps)
        # signal_maps = tf.minimum(tf.maximum(signal_maps, -280.0), -50.0)
        return signal_maps

    @cache
    def get_signal_simulations(self):
        """Get the physics based simulation of the RSSI map for each AP.

        returns: [ap, width, height, depth]
        """
        return fast_wind(
            tensors=tf.expand_dims(self.get_unwound_signal_simulations(), -1),
            offsets=self.ap_positions,
            output_shape=self.image_shape,
            radii=self.ap_radii
        )[:, :, :, :, 0]

    @cache
    def get_square_distance_rings(self):
        """Get the square distance rings for each observation for each AP.

        returns: [observation, ap, width, height, depth]"""
        rssis = tf.reshape(self.rssis, [-1, self.ap_count, 1, 1, 1])
        rssis_exists = tf.reshape(self.rssis_exists, [-1, self.ap_count, 1, 1, 1])
        signal_maps = tf.tile(
            tf.expand_dims(self.get_signal_simulations(), 0),
            [self.rssis.get_shape()[0], 1, 1, 1, 1]
        )
        return tf.square(signal_maps - rssis) * rssis_exists

    @cache
    def get_square_distance_maps(self):
        """Get the square distance maps for each observation.

        returns: [observation, width, height, depth]"""
        rings = self.get_square_distance_rings()
        return tf.reduce_sum(rings, 1)

    # @cache
    # def get_rings(self):
    #     """Get the signal rings for each observation for each AP.
    #
    #     returns: [observation, ap, width, height, depth]"""
    #     z = self.get_square_distance_rings() / self.ring_blur
    #     rings = 1 / (1 + z)
    #     rings *= tf.sigmoid(self.mutable_image)
    #     # totals = tf.reduce_sum(rings, axis=[2, 3, 4], keepdims=True)
    #     return rings  # / totals
    #
    # @cache
    # def get_location_estimations(self):
    #     """Get the location probability map for each observation.
    #
    #     returns: [observation, width, height, depth]"""
    #     location_estimations = tf.reduce_prod(self.get_rings(), 1)
    #     totals = tf.reduce_sum(location_estimations, [1, 2, 3], keepdims=True)
    #     return location_estimations / totals

    @cache
    def get_location_distribution(self):
        """Get the distribution of observation locations around the scene

        returns: [width, height, depth]"""
        return tf.reduce_sum(1 / (1 + self.get_square_distance_maps()), 0)
