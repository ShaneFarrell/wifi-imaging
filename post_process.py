from scipy.ndimage import grey_erosion, grey_dilation
import numpy as np
from skimage.filters import threshold_otsu
from scipy.ndimage import binary_dilation
from skimage.morphology import skeletonize


def post_process(estimate):
    # estimate = grey_dilation(estimate, (5, 5))
    # estimate = estimate * (estimate > threshold_otsu(estimate))
    # estimate = grey_erosion(estimate, (3, 3))

    # for _ in range(4):
    #     estimate = estimate + (estimate - gaussian_filter(estimate, sigma=7))

    estimate = estimate > threshold_otsu(estimate)

    # estimate = skeletonize(estimate)
    # estimate = binary_dilation(estimate)

    return estimate
